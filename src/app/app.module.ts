import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SapperComponent } from './sapper/sapper.component';
import { CellComponent } from './sapper/cell/cell.component';
import { ButtonComponent } from './sapper/button/button.component';

@NgModule({
  declarations: [
    AppComponent,
    SapperComponent,
    CellComponent,
    ButtonComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
