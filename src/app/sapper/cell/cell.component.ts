import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
  @Input()symbol = '';
  @Input()cell = '';
  @Output()total = new EventEmitter();
  @Output()cellClass = new EventEmitter();

  onClickCell() {
    if (this.symbol === '*') {
      this.cellClass.emit();
    }
    if (this.cell === 'win-black'){
      this.cell = 'win-black'
    }
    else {
      this.cell = '';
      this.total.emit();
    }
  }

}
