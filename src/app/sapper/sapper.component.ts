import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-sapper',
  templateUrl: './sapper.component.html',
  styleUrls: ['./sapper.component.css']
})
export class SapperComponent {
  @Input()symbol = '*';
  arrayLength = 36
  sapper: any = [];
  total = 0;
  win = '';
  splitTotal = 1;

  constructor() {
    this.creatArr();
  }

  onTotal(i: number) {
    if (this.sapper[i].cell === 'black') {
      this.sapper[i].cell = '';
      this.total ++;
      this.splitTotal ++;
    } else if (this.sapper[i].symbol === '*') {
      this.total = this.splitTotal;
      this.win = 'Congratulations you won!';
    }
  }

  creatArr() {
    const indexArr = Math.floor(Math.random() * this.arrayLength);
    for (let i = 0; i < this.arrayLength; i++) {
      const cell = {
        symbol: '',
        cell: 'black'
      };
      if (i === indexArr) {
        cell.symbol = '*';
      }
      this.sapper.push(cell);
    }
  };

  onClass() {
      for (let cell of this.sapper){
        if (cell.cell === 'black'){
          cell.cell = 'win-black';
        }
        if (cell.symbol === '*') {
          cell.cell = '';
        }
      }
  }

  onClickBtn() {
    this.sapper.splice(0, this.sapper.length);
    this.creatArr();
    this.total = 0;
    this.splitTotal = 1;
    this.win = '';
  };
}
