import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {
  @Output() click = new EventEmitter();
  total = 0;

  onClick(){
    this.total++;
    this.click.emit();
  }
}
